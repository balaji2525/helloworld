function findPermutations(p, up = "") {
  if (up.length === 0) {
    console.log(p)
    return
  }
  let ch = up[0]
  for (let i = 0; i < up.length; i++) {
    let f = up.substring(0, i)
    let s = up.substring(i, up.length)
    findPermutations(f + ch + s, up.substring(1))
  }
}

let string = "ABC"
let permutations = findPermutations(string)
console.log(permutations)
