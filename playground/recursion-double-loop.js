function triangle(i, j = 0, rowString = "") {
  if (i <= 0) {
    return
  }
  if (j < i) {
    rowString += "* "
    triangle(i, j + 1, rowString)
  } else {
    console.log(rowString)
    triangle(i - 1, 0)
  }
}

triangle(5)
