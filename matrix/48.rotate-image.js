// Solution:
// Transpose the matrix (Transpose na convert row to column or vice versa )
// Transpose pandrathuku oru mari diagonal swap nadakum
// reverse the row

var rotate = function (matrix) {
  let n = matrix.length
  for (let i = 0; i < n; i++) {
    for (let j = i; j < n; j++) {
      ;[matrix[i][j], matrix[j][i]] = [matrix[j][i], matrix[i][j]]
    }
  }
  for (let i = 0; i < n; i++) {
    for (let j = 0, k = n - 1; j < k; j++, k--) {
      ;[matrix[i][j], matrix[i][k]] = [matrix[i][k], matrix[i][j]]
    }
  }
  return matrix
}

const matrix = [
  [1, 2, 3],
  [4, 5, 6],
  [7, 8, 9],
]
console.log(rotate(matrix))
