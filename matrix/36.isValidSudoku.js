//  Ithu very intuitive tha
//  rows ellam unique ah iruka
//  columns ellam unique ah iruka
//  inner squares la unique ah iruka paakanum avlo tha

var isValidSudoku = function (board) {
  let n = board.length
  let hasValidSquares = false

  for (let i = 0; i < n; i++) {
    let hasValidRows = isValid(n, i, (x, y) => board[y][x])
    let hasValidColumns = isValid(n, i, (x, y) => board[x][y])
    if (!hasValidRows || !hasValidColumns) {
      return false
    }

    if (i % 3 === 0) {
      for (let j = 0; j < n; j += 3) {
        hasValidSquares = isValidSquare(i, j, board)
        if (!hasValidSquares) {
          return false
        }
      }
    }
  }
  return true
}

function isValidSquare(x, y, board) {
  let set = new Set()
  for (let i = x; i < x + 3; i++) {
    for (let j = y; j < y + 3; j++) {
      const val = board[i][j]
      if (val !== ".") {
        if (set.has(val)) {
          return false
        }
        set.add(val)
      }
    }
  }
  return true
}

function isValid(n, j, getCell) {
  let set = new Set()
  for (let i = 0; i < n; i++) {
    const val = getCell(i, j)
    if (val !== ".") {
      if (set.has(val)) {
        return false
      }
      set.add(val)
    }
  }
  return true
}

const board = [
  [".", ".", ".", ".", "5", ".", ".", "1", "."],
  [".", "4", ".", "3", ".", ".", ".", ".", "."],
  [".", ".", ".", ".", ".", "3", ".", ".", "1"],
  ["8", ".", ".", ".", ".", ".", ".", "2", "."],
  [".", ".", "2", ".", "7", ".", ".", ".", "."],
  [".", "1", "5", ".", ".", ".", ".", ".", "."],
  [".", ".", ".", ".", ".", "2", ".", ".", "."],
  [".", "2", ".", "9", ".", ".", ".", ".", "."],
  [".", ".", "4", ".", ".", ".", ".", ".", "."],
]
console.log(isValidSudoku(board))
