// Solution:
// rendu set, col, row set vechuko
// engellam zero varutho record pannanum
// 2nd iteration la intha rendu set index encounter aachu na assign zero very simple
var setZeroes = function (matrix) {
  let rowset = new Set()
  let colset = new Set()
  for (let i = 0; i < matrix.length; i++) {
    for (let j = 0; j < matrix[i].length; j++) {
      if (matrix[i][j] === 0) {
        rowset.add(i)
        colset.add(j)
      }
    }
  }

  for (let i = 0; i < matrix.length; i++) {
    for (let j = 0; j < matrix[i].length; j++) {
      if (rowset.has(i) || colset.has(j)) {
        matrix[i][j] = 0
      }
    }
  }
}

// first row and first column eh space kaga use pannikanum
// multiple iteration pannanum inga
// First iteration find whether row is zero or not
// Two iteration find whether column is zero or not
// Third iteration use 1st column and 1st row as space, and store the whether column is zero or not
// Fourth iteration - Make zero in the balance of the matrix
// make first row and column as zero based on first flag
var setZeroes2 = function (matrix) {
  let isFirstRowZero = false
  let isFirstColumnZero = false

  for (let i = 0; i < matrix.length; i++) {
    if (matrix[i][0] === 0) {
      isFirstColumnZero = true
    }
  }

  for (let i = 0; i < matrix[0].length; i++) {
    if (matrix[0][i] === 0) {
      isFirstRowZero = true
    }
  }

  for (let i = 0; i < matrix.length; i++) {
    for (let j = 0; j < matrix[i].length; j++) {
      if (matrix[i][j] === 0) {
        matrix[0][j] = 0
        matrix[i][0] = 0
      }
    }
  }

  for (let i = 1; i < matrix.length; i++) {
    for (let j = 1; j < matrix[i].length; j++) {
      if (matrix[0][j] === 0 || matrix[i][0] === 0) {
        matrix[i][j] = 0
      }
    }
  }

  if (isFirstColumnZero) {
    for (let i = 0; i < matrix.length; i++) {
      matrix[i][0] = 0
    }
  }

  if (isFirstRowZero) {
    for (let i = 0; i < matrix[0].length; i++) {
      matrix[0][i] = 0
    }
  }

  return matrix
}

let matrix = [
  [
    [1, 2, 3, 4],
    [5, 0, 7, 8],
    [0, 10, 11, 12],
    [13, 14, 15, 0],
  ],
]
console.log(setZeroes2(matrix))
