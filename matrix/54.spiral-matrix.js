// Solution:
// Imagine snake Game, if next index is wall direction has to be changed
// In case of spiral matrix it is always clock wise
// mark the visited node as x or v so if you encounter this it is also wall

var spiralOrder = function (matrix) {
  let [x, y] = [0, 0]
  let direction = "r"
  let totalCells = matrix[0].length * matrix.length
  let [columnLength, rowLength] = [matrix.length, matrix[0].length]
  let result = []
  while (result.length < totalCells) {
    result.push(matrix[y][x])
    matrix[y][x] = "v"
    if (direction === "r") {
      // + 1 is for indexation THIS IS CRUCIAL
      if (x + 1 === rowLength || matrix[y][x + 1] === "v") {
        y++ // INCREMENT HERE IS CRUCIAL
        direction = "d"
      } else x++
    } else if (direction === "d") {
      // + 1 is for indexation THIS IS CRUCIAL
      if (y + 1 === columnLength || matrix[y + 1][x] === "v") {
        x-- // THIS IS CRUCIAL
        direction = "l"
      } else y++
    } else if (direction === "l") {
      if (x === 0 || matrix[y][x - 1] === "v") {
        y-- // THIS IS CRUCIAL
        direction = "u"
      } else x--
    } else if (direction === "u") {
      if (y === 0 || matrix[y - 1][x] === "v") {
        x++ // THIS IS CRUCIAL
        direction = "r"
      } else y--
    }
  }
  return result
}

const matrix = [
  [1, 2, 3, 4],
  [5, 6, 7, 8],
  [9, 10, 11, 12],
]
console.log(spiralOrder(matrix))
