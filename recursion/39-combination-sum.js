/**
 * @param {number[]} candidates
 * @param {number} target
 * @return {number[][]}
 */
var combinationSum = function (candidates, target) {
  function getCombinations(result = [], start = 0, combinations = [], sum = 0) {
    if (sum === target) {
      result.push([...combinations])
      return
    }
    if (sum > target) {
      return
    }
    for (let i = start; i < candidates.length; i++) {
      combinations.push(candidates[i])
      getCombinations(result, i, combinations, sum + candidates[i])
      combinations.pop()
    }
  }
  const result = []
  getCombinations(result)
  return result
}

const candidates = [2, 3, 6, 7],
  target = 7
console.log(combinationSum(candidates, target))
