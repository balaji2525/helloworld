function combine(n, k) {
  function generateCombinations(result = [], level = 0, start = 1, combination = []) {
    if (level === k) {
      result.push([...combination])
      return
    }
    for (let i = start; i <= n; i++) {
      combination.push(i)
      generateCombinations(result, level + 1, i + 1, combination)
      combination.pop()
    }
  }
  const result = []
  generateCombinations(result)
  return result
}

const n = 4
const k = 2

console.log(combine(n, k))
