var combinationSum3 = function (k, n) {
  let arr = []
  for (let i = 1; i <= 9; i++) {
    arr.push(i)
  }

  function getCombinations(inputNos = [], combination = [], result = [], level = 0) {
    if (k === level) {
      let sum = combination.reduce((acc, item) => item + acc, 0)
      if (sum === n) {
        result.push([...combination])
      }
      return
    }

    for (let i = 0; i < inputNos.length; i++) {
      combination.push(inputNos[i])
      getCombinations(inputNos.slice(i + 1), combination, result, level + 1)
      combination.pop()
    }
  }
  const result = []
  getCombinations(arr, [], result)
  return result
}

const k = 3,
  n = 7
console.log(combinationSum3(k, n)) // [[1,2,4]]
