var coinChange = function (coins, amount) {
  if (amount === 0) return 0
  let ans = Infinity
  function getCoinChangeCombination(sum = 0, level = 0) {
    if (sum === amount) {
      return level
    }
    if (sum > amount) {
      return null
    }
    for (const coin of coins) {
      let noOflevel = getCoinChangeCombination(sum + coin, level + 1)
      if (noOflevel != null) {
        ans = Math.min(noOflevel, ans)
      }
    }
    return null
  }
  getCoinChangeCombination()
  return ans === Infinity ? -1 : ans
}

const coins = [1, 2, 5],
  amount = 11

console.log(coinChange(coins, amount))
