function factorial(i) {
  if (i <= 1) {
    return i
  }
  return i * factorial(i - 1)
}

function summation(i) {
  if (i <= 1) {
    return i
  }
  return i + factorial(i - 1)
}

function fibonacci(n) {
  let prev = 0
  let curr = 1
  let result = [0, 1]
  for (let i = 2; i < n; i++) {
    result.push(result[i - 2] + result[i - 1])
  }
  console.log(result)
}

function fibonacciR(n) {
  if (n === 0) {
    return 0
  } else if (n === 1) {
    return 1
  } else {
    let result = fibonacciR(n - 1) + fibonacciR(n - 2)
    console.log(result)
    return result
  }
}

console.log(fibonacciR(5))
