const letterCombinations = function (digits) {
  let dialpad = {
    2: "abc",
    3: "def",
    4: "ghi",
    5: "jkl",
    6: "mno",
    7: "pqrs",
    8: "tuv",
    9: "wxyz",
  }
  function generateCombinations(result = [], level = 0, combination = "") {
    if (level === digits.length) {
      return combination
    }
    const dialPadCh = digits[level]
    for (const ch of dialpad[+dialPadCh]) {
      let res = generateCombinations(result, level + 1, combination + ch)
      if (res) result.push(res)
    }
  }

  const result = []
  generateCombinations(result)
  return result
}

console.log(letterCombinations("2425254"))
