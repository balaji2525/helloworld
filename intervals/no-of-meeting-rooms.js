// https://www.geeksforgeeks.org/problems/minimum-platforms-1587115620/1?page=1&company=Google,Service%20Now&sortBy=submissions
// idea is simple arrange arrivals in the departures in the sorted array
// whenever there is an arrival increase the count and there is a departure reduce the count
// record the max count
// Solution can be further improvised by using merge two sorted array solution, so that no extra array is needed
function findPlatform(arr, dep) {
  let arrivals = arr.map((val) => ({ time: val, trip: "A" }))
  let departures = dep.map((val) => ({ time: val, trip: "D" }))
  let timeTable = [...arrivals, ...departures].sort((a, b) => a.time - b.time)
  let platformCount = 0
  let max = 0
  for (let item of timeTable) {
    if (item.trip === "A") platformCount++
    else platformCount--

    max = Math.max(max, platformCount)
  }
  return max
}

const arr = [900, 940, 950, 1100, 1500, 1800]
const dep = [910, 1200, 1120, 1130, 1900, 2000]
console.log(findPlatform(arr, dep))
