/**
 * @param {number[][]} intervals
 * @return {number[][]}
 */
var merge = function (intervals) {
  let len = intervals.length
  let result = []
  let i = 1
  let [start, end] = intervals[0]
  for (let i = 1; i < len - 1; i++) {
    let [nextStart, nextEnd] = intervals[i]
    if (nextStart <= end) {
      // Merge intervals if they overlap
      end = Math.max(end, nextEnd) // Extend the end of the current interval
    } else {
      // Push the current merged interval to the result
      result.push([start, end])
      // Start a new interval
      ;[start, end] = intervals[i]
    }
  }
  return result
}

const intervals = [
  [1, 3],
  [2, 6],
  [8, 10],
  [15, 18],
]

console.log(merge(intervals))
