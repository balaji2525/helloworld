function search(nums, target) {
  let low = 0
  let high = nums.length - 1
  //Note: <= needed
  while (low <= high) {
    mid = Math.floor((high + low) / 2)
    if (nums[mid] === target) {
      return mid
    } else if (target < nums[mid]) {
      high = mid - 1
    } else {
      low = mid + 1
    }
  }
  return -1
}

console.log(search([-1, 0, 3, 5, 9, 12], 2))
