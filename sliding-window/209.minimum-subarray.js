// https://leetcode.com/problems/minimum-size-subarray-sum/?envType=study-plan-v2&envId=top-interview-150
// Question is GREATER THAN OR EQUAL TO
// Ithu min sub array mari tha
// and Ithutha tricky pattern, sliding window nale ithu tha pattern
// for (let r = 0; r < nums.length; r++) {
//     while (condition) {
//         ...
//         l++
//     }
// }
var minSubArrayLen = function (target, nums) {
  let sum = 0
  let minLength = Infinity
  let l = 0
  for (let r = 0; r < nums.length; r++) {
    sum += nums[r]
    // greater than or equal to
    while (sum >= target) {
      minLength = Math.min(minLength, r - l + 1)
      sum -= nums[l++]
    }
  }
  return minLength !== Infinity ? minLength : 0
}

const target = 11,
  nums = [1, 2, 3, 4, 5]
console.log(minSubArrayLen(target, nums))
