// for each index you need to know what is the first occuring left max and right max
// the pick the minimum of left and right and subtract with block so we will get remaining
// https://www.youtube.com/watch?v=ZI2z5pq0TqA
function trappedRainWater(arr) {
  let n = arr.length
  let leftMax = new Array(n)
  let rightMax = new Array(n)
  leftMax[0] = arr[0];
  for (let i = 1; i < arr.length; i++) {
    leftMax[i] = Math.max(leftMax[i - 1], arr[i])
  }
  rightMax[n - 1] = arr[n - 1]
  for (let i = arr.length - 2; i >= 0; i--) {
    rightMax[i] = Math.max(arr[i], rightMax[i + 1])
  }

  let result = 0
  for (let i = 0; i < arr.length; i++) {
    result += Math.min(leftMax[i], rightMax[i]) - arr[i]
  }
  return result
}

const height = [0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1]
// const height = [4, 2, 0, 3, 2, 5]
console.log(trappedRainWater(height))
