// Leetcode link: https://leetcode.com/problems/remove-duplicates-from-sorted-array/description/?envType=study-plan-v2&envId=top-interview-150
// Solution key notes:
// Trick enna na i - 2 nyabagam vechukanum
// first type sum la -1 compare pannuvom, inga -2 kooda compare pannanum
var removeDuplicates = function (nums) {
  if (nums.length < 2) {
    return nums.length
  }
  let i = 2
  let j = 2
  while (j < nums.length) {
    if (nums[i - 2] !== nums[j]) {
      nums[i++] = nums[j]
    }
    j++
  }
  return i
}

removeDuplicates([0, 0, 1, 1, 1, 1, 2, 3, 3])
// Input: nums = [0,0,1,1,1,1,2,3,3]
// Output: 7, nums = [0,0,1,1,2,3,3,_,_]
