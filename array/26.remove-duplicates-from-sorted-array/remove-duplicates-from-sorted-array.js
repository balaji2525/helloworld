
// Leetcode link: https://leetcode.com/problems/remove-duplicates-from-sorted-array/description/?envType=study-plan-v2&envId=top-interview-150
// Solution key notes:
// in place la tha replace pannanum ana apdi yosicha solution complicated ah theriyum
// answer array onnu iruku nu ninanchu solution yosi, inplace logic anga irunthu derive pannikalam
//
// apram starting vanthu 2 index (reason 1st element is always in first place) also 2ndu element ndra bothu it is the solution
// anga logic apply panna thevai illa
// https://www.youtube.com/shorts/FVldriHfbt8
var removeDuplicates = function (nums) {
    // constrain vanthu minimum 1 size irukum
    let i = 1
    let j = 1
    while (j < nums.length) {
        // sootuchumam enna na i eh increment paneetom athana i-1 kooda tha j eh compare pannanum
        if (nums[i - 1] !== nums[j]) {
            // i eh increment pannite varanum
            nums[i++] = nums[j]
        }
        // ella iterationlayum j increment aaganum O(n) kaga
        j++
    }
    console.log(nums)
    return i
}

removeDuplicates([0, 0, 1, 1, 1, 2, 2, 3, 3, 4])
// Output: 5, nums = [0,1,2,3,4,_,_,_,_,_]
