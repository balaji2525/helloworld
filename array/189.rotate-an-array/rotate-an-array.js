// noth
var rotate = function (nums, k) {
  let n = nums.length
  // Handle cases where k is greater than the length of the array
  k = k % n

  // 7 6 5 4 3 2 1
  // reverse everything
  for (let i = 0, j = nums.length - 1; i < j; i++, j--) {
    ;[nums[j], nums[i]] = [nums[i], nums[j]]
  }
  // 7 6 5 1 2 3 4
  // reverse from kth index
  for (let i = k, j = nums.length - 1; i < j; i++, j--) {
    ;[nums[j], nums[i]] = [nums[i], nums[j]]
  }

  // 5 6 7 1 2 3 4
  // reverse from 0 to kth index
  for (let i = 0, j = k - 1; i < j; i++, j--) {
    ;[nums[j], nums[i]] = [nums[i], nums[j]]
  }
  console.log(nums)
}

var rotate2 = function (nums, k) {
  let n = nums.length
  for (let i = 0; i < k; i++) {
    let lastNum = nums[n - 1]
    for (let j = n - 1; j > 0; j--) {
      nums[j] = nums[j - 1]
    }
    nums[0] = lastNum
  }
  console.log(nums)
}

// Input: nums = [1,2,3,4,5,6,7], k = 3
// Output: [5,6,7,1,2,3,4]

rotate2([1, 2, 3, 4, 5, 6, 7], 2)
