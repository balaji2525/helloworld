// Idea is last word so iterate from last
var lengthOfLastWord = function (s) {
  if (s.length === 1) return 1
  let length = 0
  let i = s.length - 1
  while (s[i] === " " && i >= 0) i--
  while (s[i] !== " " && i >= 0) {
    length++
    i--
  }
  return length
}

const s = "Hello World"
console.log(lengthOfLastWord(s))
