// very straight forward
// https://prepfortech.io/leetcode-solutions/jump-game-ii
// chat gpt suggested
// Ithu best pattern existence kum ithu work aagum
var canJump = function (nums) {
  if (nums.length === 1) return 0
  let maxIndex = 0
  let jumps = 0
  let checkPointIndex = 0

  for (let i = 0; i < nums.length; i++) {
    maxIndex = Math.max(maxIndex, i + nums[i])
    // maxIndex paathute varanum, namma adicha maxIndex edge varikum reach paneetu edge eh marubadiyum increment pannanum
    // when zero comes in and later it is like check point,
    // iteration ageetu vanthitirukarappo maxIndex record aagirukum, appo ulla poi
    // jump eh increment pannanum
    if (i === checkPointIndex) {
      checkPointIndex = maxIndex
      jumps++
      if (maxIndex >= nums.length - 1) {
        return jumps
      }
    }
  }
}

const nums = [2, 3, 1, 1, 4]
console.log(canJump(nums))
