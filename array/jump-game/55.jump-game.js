// Ithu tricky tha debug panni paathu understand pannanum
// Explaination: https://www.youtube.com/watch?v=Yan0cv2cLy8
// enna nadakuthuna: reverse la irunthu iteration nadakuthu
// last goal eh maathi kammi pannikitte varom

// ii solution is generic, check that
var canJumpNeetCode = function (nums) {
  let n = nums.length - 1
  let goal = n
  for (let i = n; i >= 0; i--) {
    if (i + nums[i] >= goal) {
      goal = i
    }
  }
  return goal === 0
}

// ChatGPT suggested
// this is same like above only, debug and see it is super simple
var canJump = function (nums) {
  let n = nums.length - 1
  let maxReach = 0
  for (let i = 0; i <= maxReach; i++) {
    maxReach = Math.max(maxReach, i + nums[i])
    if (maxReach >= n) {
      return true
    }
  }

  return false
}

// Another idea
var canJump3 = function (nums) {
  // Set up a variable to keep track of the furthest we can jump
  let maxIndex = 0

  // Iterate through the array
  for (let i = 0; i < nums.length; i++) {
    // If the current index is greater than the furthest we can jump, return false
    if (i > maxIndex) {
      return false
    }
    // Update max to be the furthest we can jump from the current index
    maxIndex = Math.max(i + nums[i], maxIndex)
  }

  // If we reach the end, return true
  return true
}
const nums = [3, 2, 1, 0, 4]
console.log(canJump3(nums))
