// use hashmap very straight forward
var majorityElement = function (nums) {
  const map = {}
  let maxCount = 0
  let result = 0
  for (let i = 0; i < nums.length; i++) {
    if (!map[nums[i]]) {
      map[nums[i]] = 0
    }
    map[nums[i]] += 1

    if (map[nums[i]] > maxCount) {
      maxCount = map[nums[i]]
      result = nums[i]
    }
  }

  return result
}

// Check this tutorial https://www.youtube.com/watch?v=7pnhv842keE
// this is very bookish you have to MEMORIZE
// Solution key note:
//  count and result rendu variable vechukanum
//  result um iteration la irukara number um same ah iruntha count ah increment pannanum
//  count zero vanthiduchuna result number eh iteration la irukara number oda swap paneetu count increment pannanum
var moores = function (nums) {
  let count = 1
  let result = nums[0]
  for (let i = 0; i < nums.length; i++) {
    //  result um iteration la irukara number um same ah iruntha count ah increment pannanum
    if (result === nums[i]) {
      count++
      continue
    }
    count--
    //  count zero vanthiduchuna result number eh iteration la irukara number oda swap paneetu count increment pannanum
    if (count === 0) {
      result = nums[i]
      count++
      continue
    }
  }

  return result
}

console.log(majorityElement([2, 2, 1, 1, 1, 2, 2]))
console.log(majorityElement([3, 2, 3]))

console.log(moores([2, 2, 1, 1, 1, 2, 2]))
console.log(moores([3, 2, 3]))
console.log(moores([6, 5, 5]))
