// Intuitive and straightforward bruteforce
var longestCommonPrefix = function (strs) {
  let res = ""
  let minStrLen = Number.MAX_SAFE_INTEGER
  for (let str of strs) {
    minStrLen = Math.min(str.length, minStrLen)
  }
  for (let i = 0; i < minStrLen; i++) {
    let isSame = false
    for (let str of strs) {
      isSame = str.length > 0 && str[i] === strs[0][i]
      if (!isSame) break
    }
    if (isSame) {
      res += `${strs[0][i]}`
    } else {
      break
    }
  }
  return res
}

let strs = ["c", "acc", "ccc"]
console.log(longestCommonPrefix(strs))
