// Leetcode link: https://leetcode.com/problems/merge-sorted-array/description/?envType=study-plan-v2&envId=top-interview-150
// Solution key notes:
// 1. reverse for loop on the largest array
// 2. have logical condition to check j and k is greater than zero
function main(nums1, m, nums2, n) {
  let j = m - 1
  let k = n - 1
  for (let i = m + n - 1; i >= 0; i--) {
    // inga j >= 0 check pannanum, yen na first array kum 2nd array kum size differnce irukum
    // oru array seekiram loop la mudinjirum
    if (nums1[j] >= nums2[k] && j >= 0) { 
      nums1[i] = nums1[j--]
    } else if (k >= 0) {
      nums1[i] = nums2[k--]
    }
  }
  console.log(nums1)
}

main([1, 2, 3, 10, 0, 0, 0], 4, [2, 5, 6], 3)
