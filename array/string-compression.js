var compress = function (chars) {
  if (chars.length === 0) return [chars[0]]
  let ch = chars[0]
  let count = 1
  let result = []
  for (let i = 1; i < chars.length; i++) {
    if (chars[i] !== ch || i === chars.length - 1) {
      let countStrArr = `${count}`.split("")
      result.push(ch)
      for (let k = 0; k < countStrArr.length; k++) {
        result.push(countStrArr[k])
      }
      count = 1
      continue
    }
    ch = chars[i]
    count++
  }
  return result
}

const chars = ["a","b","b","b","b","b","b","b","b","b","b","b","b"]
console.log(compress(chars))
