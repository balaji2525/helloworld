const sortedSquares = function (nums) {
  let result = []
  let stack = []
  for (let i = 0; i < nums.length; i++) {
    const squared = Math.pow(nums[i], 2)
    if (nums[i] < 0) {
      stack.push(squared)
      continue
    }
    if (stack.length > 1 && stack[stack.length - 1] < squared) {
        result.push(stack.pop())
    }
    result.push(squared)
  }
  if (stack.length > 1) {
    for (let i = 0; i < stack.length; i++) {
      result.push(stack.pop())
    }
  }
  return result
}

console.log(sortedSquares([-4, -1, 0, 3, 10]))
