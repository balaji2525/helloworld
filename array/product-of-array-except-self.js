// leet code link: https://leetcode.com/problems/product-of-array-except-self/
// Solution: https://www.youtube.com/watch?v=G9zKmhybKBM

var productExceptSelf = function (nums) {
  const result = new Array(nums.length)
  const lastIndex = nums.length
  // On left to right product of all the other is calculated execpt self towards right
  let product = 1
  for (let i = 0; i < lastIndex; i++) {
    result[i] = product
    product = product * nums[i]
  }

  product = 1
  // On right to left product of all the other is calculated execpt self towards left
  for (let i = lastIndex - 1; i >= 0; i--) {
    result[i] = product * result[i]
    product = product * nums[i]
  }

  return result
}

// understanding ku ithu best, optimization vanthu above one
var productExceptSelfPractixe = function (nums) {
  const ltrSum = []
  let product = 1
  for (let i = 0; i < nums.length; i++) {
    ltrSum.push(product)
    product *= nums[i]
  }
  // [1, 2, 6, 24]
  const rtlSum = new Array(nums.length)
  product = 1
  for (let i = nums.length - 1; i >= 0; i--) {
    rtlSum[i] = product
    product *= nums[i]
  }
  // [24, 24, 12, 4]

  const result = []
  for (let i = 0; i < nums.length; i++) {
    result.push(ltrSum[i] * rtlSum[i])
  }
  return result
}

console.log(productExceptSelfPractixe([1, 2, 3, 4]))
