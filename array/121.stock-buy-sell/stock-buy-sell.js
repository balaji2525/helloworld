// https://leetcode.com/problems/best-time-to-buy-and-sell-stock/description/?envType=study-plan-v2&envId=top-interview-150
// Solution key point:
// Problem is about BEST SINGLE TRANSACTION in the entire array
// This is very simple, run the simulation
// always buy at low price, and whenever max profit is detected bump up the max profit
function maxProfit(prices) {
  let buyPrice = Number.MAX_SAFE_INTEGER
  let maxProfit = Number.MIN_SAFE_INTEGER
  for (let currentPrice of prices) {
    // Best buy price is more that the current price
    if (buyPrice > currentPrice) {
      buyPrice = currentPrice
    }
    let presentProfit = currentPrice - buyPrice
    // comparing the present profit with the max profit
    if (presentProfit > maxProfit) {
      maxProfit = currentPrice - buyPrice
    }
  }
  return maxProfit
}
console.log(maxProfit([7, 2, 5, 3, 7, 4, 1, 10]))
