// https://leetcode.com/problems/best-time-to-buy-and-sell-stock/description/?envType=study-plan-v2&envId=top-interview-150
// Solution key point:
// This is very simple, run the simulation
// blindly
//  1. buy today and sell on second day
//  2. buy again on second day
//  3. sell it on next profit
function maxProfit(prices) {
  let maxProfit = 0
  let buyPrice = prices[0]
  for (let i = 1; i < prices.length; i++) {
    if (buyPrice < prices[i]) {
      maxProfit += prices[i] - buyPrice
    }
    buyPrice = prices[i]
  }
}
console.log(maxProfit([7, 1, 5, 10, 3, 6, 4]))
