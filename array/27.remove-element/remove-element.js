// Leetcode link: https://leetcode.com/problems/remove-element/description/?envType=study-plan-v2&envId=top-interview-150
// Solution key notes:
// Have a pointer from behind "k"
// Reverse loop
var removeElement = function (nums = [], val) {
  let k = nums.length - 1
  let i = 0
  // use single loop
  // inga  >= mukkiyam ***
  while (k >= i) {
    // last la irukra value val ku equal ah iruntha decrement pannete varanum
    if(nums[k] === val) {
      k--
      continue
    }
    // nums[i] ku equal ah val iruntha nums[k] thooki i index la potranum
    // swap pannanum nu avasiyam illa
    if (nums[i] === val) {
      nums[i] = nums[k]
      k--
    }
    i++
  }
  // return i mukiyam ***
  return i
}

console.log(removeElement([0, 1, 2, 2, 3, 0, 4, 2], 2))
// console.log(removeElement([3, 2, 2, 3], 3))
