// Solution is almost bruteforce
var strStr = function (haystack, needle) {
  if (haystack.length < needle.length) {
    return -1
  }
  let j = 0
  for (let i = 0; i < haystack.length; i++) {
    if (haystack[i] === needle[j]) {
      j++
      if (j === needle.length) {
        return i - (j - 1)
      }
    } else {
      // reset the counter
      i = i - j
      j = 0
    }
  }
  return -1
}

/**
 * Uses extra space
 * store the first character in the array & conditionally check if the differnce between prev and next index is greater or equal to length of needle
 * use double loop and check the matches
 */
var strStr2 = function (haystack, needle) {
  // store the starting index
  let startIndex = []
  // iteration one
  for (let i = 0; i < haystack.length; i++) {
    if (haystack[i] === needle[0]) {
      // remove if the difference between previous index and current index is less than size of needle
      if (haystack.at(-1) - i < needle.length) {
        startIndex.pop()
      }
      startIndex.push(i)
    }
  }

  // iteration two
  // following may look n2 but it is not, it is
  for (let start of startIndex) {
    let j = 0
    let i = start
    while (j < needle.length) {
      if (haystack[i] !== needle[j]) {
        break
      }
      i++
      j++
    }
    if (j === needle.length) {
      return start
    }
  }
  return -1
}

var strStr3 = function (haystack, needle) {
  let i = 0
  let j = 0
  while (i < haystack.length) {
    let k = 0
    while (haystack[i++] === needle[j++] && j < needle.length) {
      k++
    }
    if (k !== needle.length - 1) {
      i = i - k
      j = 0
      continue
    }
    return i - k - 1
  }
  return -1
}

// const haystack = "mississippi",
//   needle = "issip"

const haystack = "sadbutsad",
  needle = "sad"
strStr3(haystack, needle)
