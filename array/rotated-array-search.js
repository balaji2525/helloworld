
// Leetcode link: https://leetcode.com/problems/search-in-rotated-sorted-array/
// Solution: 
// Key: it is a binary search with extra conditions, Idea is always either one side will sorted
function search(nums, target) {
  let low = 0
  let high = nums.length - 1
  while (low <= high) {
    mid = Math.floor((high + low) / 2)
    if (nums[mid] === target) {
      return mid
    }

    // Idea is always either one side will sorted
    if (nums[low] <= nums[mid]) {
      if (nums[low] <= target && target <= nums[mid]) {
        high = mid - 1
      } else {
        low = mid + 1
      }
    } else {
      if (nums[mid] <= target && target <= nums[high]) {
        low = mid + 1
      } else {
        hight = mid - 1
      }
    }
  }
  return -1
}

console.log(search([5, 1, 3], 0))
