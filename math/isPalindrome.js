var isPalindrome = function (x) {
  if (x < 0) {
    return false
  }
  let result = 0
  let val = x
  while (x > 0) {
    // here doing x 10 because making space for last digit alone
    result = (x % 10) + result * 10
    x = Math.floor(x / 10)
  }
  return val === result
}

isPalindrome(10)
