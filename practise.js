// https://leetcode.com/problems/summary-ranges/?envType=study-plan-v2&envId=top-interview-150
// Question is to ignore the gaps and merge the ranges (check the discussion section)
// Just used intuition

var summaryRanges = function (nums) {
  // start index
  let i = 0
  // current index
  let j = 0
  let result = []
  while (j < nums.length) {
    let next = nums[j] + 1

    // j + 1 checks the continuity
    if (nums[j + 1] !== next) {
      // when j and i are equal then it is a single number
      if (j === i) {
        result.push(`${nums[i]}`)
      } else {
        // when j & i are not equal then it is a proper range
        result.push(`${nums[i]}->${nums[j]}`)
      }
      // assign j+1 to i because i has to start from next number
      i = j + 1
    }
    j++
  }
  return result
}

const nums = [0, 2, 3, 4, 6, 8, 9]

console.log(summaryRanges(nums))
