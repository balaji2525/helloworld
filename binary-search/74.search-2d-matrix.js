// Matrix search na onnum perusu illa co ordinates kandu pudicha pothum same logic
var searchMatrix = function (matrix, target) {
  let row = matrix[0].length
  let col = matrix.length
  let n = row * col
  let l = 0
  let r = n - 1
  let mid
  while (l <= r) {
    mid = Math.floor((l + r) / 2)
    let { x, y } = getCoordinates(mid, row)
    if (matrix[y][x] === target) return true
    else if (matrix[y][x] < target) {
      l = mid + 1
    } else {
      r = mid - 1
    }
  }
  return false
}

function getCoordinates(index, rowLen) {
  let x = index % rowLen
  let y = Math.floor(index / rowLen)
  return { x, y }
}
