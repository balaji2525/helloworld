/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */
var searchInsert = function (nums, target) {
  let n = nums.length
  let l = 0,
    r = n - 1
  let mid
  while (r >= l) {
    mid = Math.floor((l + r) / 2)
    if (nums[mid] === target) return mid
    if (nums[mid] < target) {
      l = mid + 1 // Move the left pointer to mid + 1, yenna mid pointer already check panniyachu, so move away
    } else {
      r = mid - 1 // Move the right pointer to mid - 1
    }
  }
  return l // yenna enga insert panna porondrathu iruku la
}

let nums = [1, 3, 4, 5, 6, 8, 9],
  target = 3
console.log(searchInsert(nums, target))
