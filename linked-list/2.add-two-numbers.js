var addTwoNumbers = function (l1, l2) {
  let curr = null
  let carry = 0
  let head = null
  while (l1 || l2) {
    let l1Val = l1 ? l1.val : 0
    let l2Val = l2 ? l2.val : 0
    let sum = l1Val + l2Val + carry
    carry = Math.floor(sum / 10)
    let node = {
      val: sum % 10,
      next: null,
    }
    if (!head) {
      head = node
      curr = head
    } else {
      curr.next = node
      curr = curr.next
    }
    if (l1) l1 = l1.next
    if (l2) l2 = l2.next
  }
  if (carry > 0) {
    curr.next = {
      val: carry,
      next: null,
    }
  }
  return head
}

const l1 = [2, 4, 3],
  l2 = [5, 6, 4]

console.log(addTwoNumbers(linkedList(l1), linkedList(l2)))

// This is crucial creating a linked list from scratch
function linkedList(ll) {
  let head
  let curr = head
  ll.forEach((val) => {
    let node = {
      val: val,
      next: null,
    }
    if (!head) {
      head = node
      curr = head
    } else {
      curr.next = node
      curr = curr.next
    }
  })
  return head
}
