var mergeTwoLists = function (list1, list2) {
  if (list1 !== null && list2 !== null) {
    if (list1.val < list2.val) {
      list1.next = mergeTwoLists(list1.next, list2)
    } else {
      list2.next = mergeTwoLists(list1, list2.next)
    }
  }
  return list1 || list2
}

// var mergeTwoLists = function (list1, list2) {
//   let curr
//   let head
//   while (list1 || list2) {
//     if (!list1 || !list2) {
//       return head
//     }
//     let val = 0
//     if (list1 && list1.val <= list2.val) {
//       val = list1.val
//       list1 = list1.next
//     } else {
//       val = list2.val
//       list2 = list2.next
//     }
//     let node = {
//       val,
//       next: null,
//     }

//     if (!head) {
//       head = node
//       curr = head
//       continue
//     }
//     curr.next = node
//     curr = curr.next
//   }
//   return head
// }

const list1 = [1, 2, 4],
  list2 = [1, 3, 4]

console.log(mergeTwoLists(linkedList(list1), linkedList(list2)))

// This is crucial creating a linked list from scratch
function linkedList(ll) {
  let head
  let curr = head
  ll.forEach((val) => {
    let node = {
      val: val,
      next: null,
    }
    if (!head) {
      head = node
      curr = head
    } else {
      curr.next = node
      curr = curr.next
    }
  })
  return head
}
