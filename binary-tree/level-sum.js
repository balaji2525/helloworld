// This is simple traversal thing, just note that you need to add shifted node's value
var maxLevelSum = function (root) {
  let queue = [root]
  let maxLevelSum = -Infinity
  let maxLevel = 1
  let level = 1
  while (queue.length !== 0) {
    let levelSum = 0
    let len = queue.length
    for (let i = 0; i < len; i++) {
      const { val, left, right } = queue.shift()
      levelSum += val // THIS IS TO NOTE
      if (left) {
        queue.push(left)
      }
      if (right) {
        queue.push(right)
      }
    }
    if (levelSum > maxLevelSum) {
      maxLevelSum = levelSum
      maxLevel = level
    }
    level++
  }
  return maxLevel
}
