var countNodes = function (root) {
  if (!root) {
    return 0
  }
  let l = countNodes(root.left)
  let r = countNodes(root.right)
  return 1 + l + r
}
