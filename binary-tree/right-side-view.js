var rightSideView = function (root) {
  if (!root) return []
  let queue = []
  const result = []
  queue.push(root)
  result.push(root.val)
  while (queue.length !== 0) {
    let len = queue.length
    for (let i = 0; i < len; i++) {
      let item = queue.shift()
      if (item.left) queue.push(item.left)
      if (item.right) queue.push(item.right)
    }
    if (queue.length !== 0) result.push(queue.at(-1).val)
  }
  return result
}

let root = {
  val: 1,
  left: {
    val: 2,
    right: {
      val: 5,
    },
  },
  right: {
    val: 3,
    right: {
      val: 4,
    },
  },
}

console.log(rightSideView(root))
