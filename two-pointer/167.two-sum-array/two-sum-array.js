// brute force
var bruteForce = function (numbers, target) {
  for (let i = 0; i < numbers.length; i++) {
    if (numbers[i] > target) break
    let balance = target - numbers[i]
    for (let j = i + 1; j < numbers.length; j++) {
      if (numbers[j] > balance) break
      if (numbers[j] === balance) {
        return [i + 1, j + 1]
      }
    }
  }
  return []
}

// Leet code link: https://leetcode.com/problems/two-sum-ii-input-array-is-sorted/?envType=study-plan-v2&envId=top-interview-150
// Super easy tutorial: https://www.youtube.com/watch?v=cQ1Oz4ckceM
// Solution key notes:
//   ascending order la irukrathu advantage
//   "target" eh vida "sum perusa iruntha" right eh decrement pannanum
//   "target" eh vida "sum sirusa iruntha" left eh increment pannanum
var twoSum = function (numbers, target) {
  let left = 0
  let right = numbers.length - 1
  while (left < right) {
    let sum = numbers[right] + numbers[left]
    if (sum > target) {
      right--
    } else if (sum < target) {
      left++
    } else {
      return [left + 1, right + 1]
    }
  }
  return []
}
