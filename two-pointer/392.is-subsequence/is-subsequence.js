// Solution key notes:
// Iterate through longer string, increment shorter string whenever there is a match
// it is very straight forward
var isSubsequence = function (s, t) {
  let i = 0
  let j = 0
  while (j < t.length && i < s.length) {
    if (s.charAt(i) === t.charAt(j)) {
      i++
    }
    j++
  }
  console.log(i === s.length)
}

isSubsequence("abc", "ahbgdc")
isSubsequence("axc", "ahbgdc")
