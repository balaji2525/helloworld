function findLongestSubstring(str) {
  let maxLength = 0
  let set = new Set()
  let i = 0
  let k = 0
  while(i < str.length){
      while(set.has(str[i])) {
        set.delete(str[k++])
        k++
      }
      set.add(str[i++])
      maxLength = Math.max(maxLength, set.size)
  }
  return maxLength
}
const str = "qrsvbspk"
console.log(findLongestSubstring(str))
