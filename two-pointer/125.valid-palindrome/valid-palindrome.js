// This is very straight forward no explaination needed
var isPalindrome = function (s) {
  s = s.replace(/[^a-zA-Z0-9]/g, "").toLowerCase()
  for (let i = 0, j = s.length - 1; i < j; i++, j--) {
    if (s.charAt(j) !== s.charAt(i)) {
      return false
    }
  }
  return true
}

isPalindrome("ab_a")