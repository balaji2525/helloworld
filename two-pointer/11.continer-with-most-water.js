/**
 * Ithu romba easy the typical two-pointer pattern use pannitu
 * https://www.youtube.com/watch?v=UuiTKBwPgAo
 * left right la ethu kammiya iruko adha increment pannanum
 */
var maxArea = function (height) {
  let i = 0
  let j = height.length - 1
  let maxArea = 0

  while (i < j) {
    // Length = j - i, breadth = min(height[i], height[j])
    let area = (j - i) * Math.min(height[i], height[j])
    maxArea = Math.max(area, maxArea)

    if (height[i] < height[j]) {
      i++
    } else {
      j--
    }
  }
  return maxArea
}
