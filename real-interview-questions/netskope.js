/*

Write a function that accepts a string representation of a chemical compound and returns a JSON object containing KV pairs where keys are the element symbols and the values are the element counts.

"H2O" => {"H": 2, "O": 1}
"C6H12" => {"C": 6, "H": 12}
"COOH" => {"C": 1, "O": 2, "H": 1}

*/

// 1. character follower by Number
// 2. if no trailing number then it has to considered 1
// 3. There can be repeatation

// let str = ['C', 6, 'H' , 1, 2]
// let str = ['C', 'O', 'O' , 'H']
// let str = ['H', 2, 'O']
let str = "COOHO5"
let map = {}
for( let i = 0; i< str.length; i++) {
  let ch = str[i]
  let individualCount = ''
  let j = i + 1
  while(!isNaN(parseInt(str[j]))) {
    individualCount +=  `${str[j]}`
    j++
    i++
  }
  if(individualCount === '') {
    individualCount = '1'
  }

  if(map[ch]){
    map[ch] = parseInt(map[ch]) + parseInt(individualCount)
    continue
  }
  map[ch] = individualCount
}
console.log(map)
// let ch = isNaN(parseInt('2'))
// console.log(ch)