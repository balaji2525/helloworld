var numIslands = function (grid) {
  let count = 0
  for (let y = 0; y < grid.length; y++) {
    for (let x = 0; x < grid[0].length; x++) {
      if (grid[y][x] === "1") {
        count++
        markVisited(grid, x, y)
      }
    }
  }
  return count
}

function markVisited(grid, x, y) {
  let length = grid[0].length - 1
  let height = grid.length - 1
  if (x < 0 || y < 0 || x > length || y > height || grid[y][x] === "0" || grid[y][x] === "v") {
    return
  }
  // mark the indexes where I visited
  grid[y][x] = "v"
  // check the boundaries and make all the liked ones as visited
  markVisited(grid, x - 1, y)
  markVisited(grid, x + 1, y)
  markVisited(grid, x, y - 1)
  markVisited(grid, x, y + 1)
}

const grid = [
  ["1", "1", "0", "0", "0"],
  ["1", "1", "0", "0", "0"],
  ["0", "0", "1", "0", "0"],
  ["0", "0", "0", "1", "1"],
]

console.log(numIslands(grid))
