// Best tutorial https://www.youtube.com/watch?v=y704fEOx0s0
// first rotten orange ellam eduthu queue la potranum
// BFS algo padi Q ZERO length ku oru iteration
// queue ku oru iteration, the boundaries check pannanum
var orangesRotting = function (grid) {
  let queue = []
  let freshOranges = 0
  for (let y = 0; y < grid.length; y++) {
    for (let x = 0; x < grid[0].length; x++) {
      if (grid[y][x] === 2) {
        queue.push({ x, y })
      } else if (grid[y][x] === 1) {
        freshOranges++
      }
    }
  }
  let time = 0
  let directions = [
    { x: 1, y: 0 },
    { x: -1, y: 0 },
    { x: 0, y: 1 },
    { x: 0, y: -1 },
  ]
  while (queue.length > 0 && freshOranges > 0) {
    let isRotten = false
    let queueCount = queue.length
    for (let i = 0; i < queueCount; i++) {
      let { x, y } = queue.shift()
      for (let direction of directions) {
        let [dx, dy] = [x + direction.x, y + direction.y]
        if (dx < 0 || dy < 0 || dx >= grid[0].length || dy >= grid.length || grid[dy][dx] !== 1) {
          continue
        }
        grid[dy][dx] = 2
        queue.push({ x: dx, y: dy })
        freshOranges--
        isRotten = true
      }
    }
    if (isRotten) time++
  }
  return freshOranges === 0 ? time : -1
}

const grid = [
  [2, 1, 1],
  [1, 1, 0],
  [0, 1, 1],
]

console.log(orangesRotting(grid))
