// Same like numIslands but just have the condition in the iteration
var solve = function (board) {
  if (board.length === 0) return
  let length = board[0].length
  let height = board.length

  // Start by marking all 'O's connected to the border
  for (let y = 0; y < height; y++) {
    for (let x = 0; x < length; x++) {
      // DIFFERENCE - edge la irunthu traverse aagi ulla ellam visited mark pannanum
      // Mitchathellam surrounded tha, so mark it X
      if ((x === 0 || y === 0 || x === length - 1 || y === height - 1) && board[y][x] === "O") {
        markVisited(board, x, y)
      }
    }
  }

  // Now, flip all remaining 'O's to 'X' and revert 'V' back to 'O'
  for (let y = 0; y < height; y++) {
    for (let x = 0; x < length; x++) {
      if (board[y][x] === "V") {
        board[y][x] = "O"
      } else if (board[y][x] === "O") {
        board[y][x] = "X"
      }
    }
  }

  return board
}

function markVisited(board, x, y) {
  let length = board[0].length - 1
  let height = board.length - 1

  if (x < 0 || y < 0 || x > length || y > height || board[y][x] === "X" || board[y][x] === "V") {
    return
  }

  board[y][x] = "V"

  // Recursively mark all connected 'O's
  markVisited(board, x - 1, y)
  markVisited(board, x + 1, y)
  markVisited(board, x, y - 1)
  markVisited(board, x, y + 1)
}

const board = [
  ["X", "X", "X", "X"],
  ["X", "O", "O", "X"],
  ["X", "O", "O", "X"],
  ["X", "O", "X", "X"],
]

console.log(solve(board))
