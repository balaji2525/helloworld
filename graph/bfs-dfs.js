const graph = {
  a: ["b", "c"],
  b: ["d"],
  c: ["e"],
  d: ["f"],
  e: [],
  f: [],
}

const dfs = function (graph, node) {
  const stack = [node]
  while (stack.length > 0) {
    const current = stack.shift()
    console.log(current)
    for (let item of graph[current]) {
      stack.push(item)
    }
  }
}

dfs(graph, "a")
