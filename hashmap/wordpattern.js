// Ithu same isomorphic logic than
var wordPattern = function (pattern, s) {
  let map = {}
  let arr = s.split(" ")
  if (pattern.length !== arr.length) {
    return false
  }

  // duplicates remove panni patha same length irukanum
  let sSet = new Set(arr)
  let pSet = new Set(pattern.split(""))
  if (sSet.size !== pSet.size) {
    return false
  }

  // single iteration pattern
  for (let i = 0; i < pattern.length; i++) {
    let ch = pattern[i]
    // existence check pannanum
    if (map[ch] && map[ch] !== arr[i]) {
      return false
    }
    map[ch] = arr[i]
  }
  return true
}

// Example 1:

// Input: pattern = "abba", s = "dog cat cat dog"
// Output: true
// Example 2:

// Input: pattern = "abba", s = "dog cat cat fish"
// Output: false
// Example 3:

// Input: pattern = "aaaa", s = "dog cat cat dog"
// Output: false

const pattern = "abba"
const s = "dog cat cat dog"

wordPattern(pattern, s)
