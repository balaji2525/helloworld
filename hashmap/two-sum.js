
// store all the values as key and index as value in the map
// check whether complement exist in the map
// if yes return both the indexes
// Note: donot use {} for the map, if(map[1] = 0) would fail
var twoSum = function (nums, target) {
  let map = new Map()
  for (let i = 0; i < nums.length; i++) {
    const complement = target - nums[i]
    if (map.has(complement)) {
      return [map.get(complement), i]
    }
    map.set(nums[i], i)
  }
  return []
}

const nums = [2, 7, 11, 15],
  target = 9
console.log(twoSum(nums, target))
