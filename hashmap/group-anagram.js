// THIS IS NOT ABOUT SOLVING ANAGRAM & GROUPING
// JUST GROUP THEM IT IS ENOUGH
var groupAnagrams = function (strs) {
    const map = {}
    for (const str of strs) {
        const sortedStr = str.split("").sort().join("")
        if (!map[sortedStr]) {
            map[sortedStr] = []
        }
        map[sortedStr].push(str)
    }
    return Object.values(map)
}