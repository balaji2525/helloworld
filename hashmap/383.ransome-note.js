// Very straigh forward
var canConstruct = function (ransomNote, magazine) {
  if (ransomNote.length > magazine.length) false

  let magMap = {}
  for (const ch of magazine) {
    if (!magMap[ch]) {
      magMap[ch] = 0
    }
    magMap[ch] += 1
  }

  let i = 0
  for (const ch of ransomNote) {
    if (magMap[ch] > 0) {
      magMap[ch]--
      i++
    }
  }
  return ransomNote.length === i
}

const ransomNote = "aa",
  magazine = "aab"
console.log(canConstruct(ransomNote, magazine))
