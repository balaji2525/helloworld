// keep storing the value & index in the map
// every iteration check the condition of the presence and the window
// it is easy
var containsNearbyDuplicate = function (nums, k) {
  let map = new Map()
  for (let i = 0; i < nums.length; i++) {
    if (map.has(nums[i]) && i - map.get(nums[i]) <= k) {
      return true
    }
    map.set(nums[i], i)
  }
  return false
}

console.log(containsNearbyDuplicate([0, 1, 2, 3, 2, 5], 3))
