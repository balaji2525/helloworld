var isIsomorphic = function (s, t) {
  if (s.length !== t.length) {
      return false
  }
  let sMap = {}
  let tMap = {}
  for (let i = 0; i < s.length; i++) {
      sMap[s[i]] = t[i]
  }

  for (let i = 0; i < t.length; i++) {
      tMap[t[i]] = s[i]
  }

  let result = []
  for (let i = 0; i < s.length; i++) {
      result.push(sMap[s[i]])
  }

  let result2 = []
  for (let i = 0; i < t.length; i++) {
      result2.push(tMap[t[i]])
  }

  return result.join("") === result2.join("")
}
isIsomorphic("egg", "add")