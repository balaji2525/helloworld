var isAnagram = function(s, t) {
    let map = {}
    for(let ch of s) {
        if(!map[ch]) {
            s[ch] = 0
        }
        map[ch]++
    }
    for(let ch of t) {
        if(!map[ch]) {
            return false
        }
        map[ch]--
        if(map[ch] === 0) {
            delete map[ch]
        }
    }
    return Object.values(map).length === 0
};

const s = "anagram", t = "nagaram"
isAnagram(s, t)