// THIS PROBLEM IS ALL ABOUT SKIPPING THE ITERATION
// if (set.has(element - 1)) continue
// is the trick
// Correct
var longestConsecutive = function (nums) {
  let set = new Set(nums)
  let max = 0
  for (const element of nums) {
    // Only start when there's no next small number [DEBUG AND SEE YOU WILL UNDERSTAND] (reason at 3)
    if (set.has(element - 1)) continue
    let j = 0
    let num = element
    while (set.has(num)) {
      num++
      j++
    }
    max = Math.max(j, max)
  }
  return max
}

// Correct
var longestConsecutive2 = function (nums) {
  let set = new Set(nums)
  let max = 0

  for (const element of nums) {
    // Only start when there's no immediate next number [DEBUG AND SEE YOU WILL UNDERSTAND] (reason at 3)
    if (set.has(element + 1)) continue
    let num = element - 1
    let j = 1
    while (set.has(num)) {
      j = j + 1
      num--
    }
    max = Math.max(j, max)
  }
  return max
}

// Takes more time than the above solution, thats y only start when there's no smaller number  (start the count from the smallest number)
// it can be done vice versa also
var longestConsecutive3 = function (nums) {
  let set = new Set(nums)
  let max = 0
  let j = 1
  for (const element of nums) {
    let num = element - 1
    while (set.has(num)) {
      j = j + 1
      num--
    }
    max = Math.max(j, max)
    j = 1
  }
  return max
}

const nums = [100, 4, 200, 1, 3, 2]

console.log(longestConsecutive2(nums))
