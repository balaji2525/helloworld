// Clue to remember is squaredValue can repeat
// so save it in set and check its existence
var isHappy = function (n) {
  let squaredValue = n
  let set = new Set()
  do {
    squaredValue = getSquaredValue(squaredValue)
    if (set.has(squaredValue)) {
      return false
    }
    set.add(squaredValue)
  } while (squaredValue !== 1)
  return true
}

// This is obvious
function getSquaredValue(n) {
  let val = n
  let sum = 0
  while (val > 0) {
    sum += (val % 10) * (val % 10)
    val = Math.floor(val / 10)
  }
  return sum
}

console.log(isHappy(19))
