var isValid = function (s) {
  let map = {
    "(": ")",
    "{": "}",
    "[": "]",
  }
  let stack = []
  for (let ch of s) {
    if (ch === stack.at(-1)) {
      stack.pop()
      continue
    }
    stack.push(map[ch])
  }
  return stack.length === 0
}
